//
//  MovsUITests.swift
//  MovsUITests
//
//  Created by Adolfho Athyla on 03/07/2018.
//  Copyright © 2018 a7hyla. All rights reserved.
//

import XCTest

class MovsUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testGeneral() {
        
        let app = XCUIApplication()
        sleep(3)
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery.children(matching: .cell).element(boundBy: 0).buttons["favorites alt"].tap()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 3).children(matching: .other).element.tap()
        
        let tablesQuery = app.tables
        let genresStaticText = tablesQuery.staticTexts["genres"]
        genresStaticText.swipeUp()
        
        let movsMoviedetailtableviewNavigationBar = app.navigationBars["Movs.MovieDetailTableView"]
        let favoritesButton = movsMoviedetailtableviewNavigationBar.buttons["favorites"]
        favoritesButton.tap()
        
        let moviesButton = movsMoviedetailtableviewNavigationBar.buttons["Movies"]
        moviesButton.tap()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 2).children(matching: .other).element.children(matching: .other).element.tap()
        favoritesButton.tap()
        moviesButton.tap()
        
        app.tabBars.buttons["Favorites"].tap()
        
        app.tabBars.buttons["Genres"].tap()
        
        let tablesQuery2 = app.tables
        tablesQuery2.children(matching: .cell).element(boundBy: 0).staticTexts["Jurassic World: Fallen Kingdom"].tap()
        tablesQuery2.staticTexts["title"].tap()
        
    }
    
    func testDateFilter() {
        
        let app = XCUIApplication()
        app.tabBars.buttons["Favorites"].tap()
        app.navigationBars["Favorites"].children(matching: .button).element(boundBy: 1).tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Date"]/*[[".cells.staticTexts[\"Date\"]",".staticTexts[\"Date\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["2018"]/*[[".cells.staticTexts[\"2018\"]",".staticTexts[\"2018\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.buttons["Apply"].tap()

    }
    
    
    func testGenreFilter() {
        
        let app = XCUIApplication()
        app.tabBars.buttons["Favorites"].tap()
        
        let favoritesNavigationBar = app.navigationBars["Favorites"]
        favoritesNavigationBar.children(matching: .button).element(boundBy: 1).tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Genre"]/*[[".cells.staticTexts[\"Genre\"]",".staticTexts[\"Genre\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Adventure"]/*[[".cells.staticTexts[\"Adventure\"]",".staticTexts[\"Adventure\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.buttons["Apply"].tap()

        
    }
    
}
