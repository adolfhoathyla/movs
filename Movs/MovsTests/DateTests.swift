//
//  DateTests.swift
//  MovsTests
//
//  Created by Adolfho Athyla on 03/07/2018.
//  Copyright © 2018 a7hyla. All rights reserved.
//

import XCTest
@testable import Movs

class DateTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testDateSourcedInServer() {
        let dateServer = Date.dateSourcedInServer(dateString: "2018-08-11")
        let dateL = Date.generateDate(dateString: "2018-08-11")
        XCTAssertEqual(dateServer, dateL)
    }
    
    func testDateString() {
        let date = Date.generateDate(dateString: "2017-04-26")
        XCTAssertEqual("2017-04-26", date.stringDate())
    }
    
    func testDateYear() {
        let date = Date.generateDate(dateString: "2014-06-24")
        XCTAssertEqual(2014, date.year())
    }
    
}
